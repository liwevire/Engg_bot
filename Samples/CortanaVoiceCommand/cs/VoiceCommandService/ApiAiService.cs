﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ApiAiSDK;
using ApiAiSDK.Model;

namespace AdventureWorks
{
    class ApiAiService
    {

        public async Task<string> getResult (string speechInput)
        {
            string result = "";
            try
            {
                ApiAi apiAi;
                //var aiConfiguration = new AIConfiguration("664f0a9c949842d3a1f8a18c70407285", SupportedLanguage.English);
                var aiConfiguration = new AIConfiguration("763dbe07d1dd4dd7b110c443cd1bf6d8", SupportedLanguage.English);
                
                apiAi = new ApiAi(aiConfiguration);
                var apiAiResponse = await apiAi.TextRequestAsync(speechInput);
                //Console.WriteLine(apiAiResponse.Result.Fulfillment.Speech);
                //Console.ReadLine();
                result = apiAiResponse.Result.Fulfillment.Speech.ToString();
                //result = "This is great, my Balance is 100 dollars";
            }
            catch (Exception ex)
            {
                ex.ToString();
                result = "Unable to connect A p i A i service";
            }
            return result;
        }
    }
}
