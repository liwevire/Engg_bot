﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Windows.Media.Core;
using Windows.Media.Playback;
using Windows.Media.SpeechSynthesis;
using Windows.Media.SpeechRecognition;
using Windows.UI.Xaml.Controls;

namespace AdventureWorks
{
    class PlayBackService
    {
        SpeechSynthesizer synthesizer = new SpeechSynthesizer();
        MediaPlayer player = new MediaPlayer();
        MediaElement media = new MediaElement();
        

        public async void PlayMedia(string result)
        {
//            result = "engineering services has I O T, contact center. Since we're expecting to always show a details page, navigate even if. Navigate to either the main trip list page, or if a valid voice commandengineering services has I O T, contact center. Since we're expecting to always show a details page, navigate even if. Navigate to either the main trip list page, or if a valid voice command";

            //player.AutoPlay = false;
            //player.MediaEnded += media_MediaEnded;

            if (media.CurrentState == Windows.UI.Xaml.Media.MediaElementState.Playing)
            {
                media.Stop();
            }

            try
            {
                // Create a stream from the text. This will be played using a media element.

                SpeechSynthesisStream synthesisStream = await synthesizer.SynthesizeTextToStreamAsync(result);

                media.AutoPlay = true;
                media.SetSource(synthesisStream, synthesisStream.ContentType);
                media.Play();
            }
            catch (Exception ex)
            {
                var messageDialog1 = new Windows.UI.Popups.MessageDialog(ex.ToString());
                // If the SSML stream is not in the correct format, throw an error message to the user.
                //btnSpeak.Content = "Speak";
                var messageDialog = new Windows.UI.Popups.MessageDialog("Unable to synthesize text");
                await messageDialog.ShowAsync();
            }
        }
    }
}
